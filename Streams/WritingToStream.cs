using System.Globalization;
using System.Text;

namespace Streams
{
    public static class WritingToStream
    {
        public static void ReadAndWriteIntegers(StreamReader streamReader, StreamWriter outputWriter)
        {
            while (!streamReader.EndOfStream)
            {
                int character = streamReader.Read();
                outputWriter.Write(character);
            }
        }

        public static void ReadAndWriteChars(StreamReader streamReader, StreamWriter outputWriter)
        {
            char character;
            while (!streamReader.EndOfStream)
            {
                character = (char)streamReader.Read();
                if (char.IsLetter(character))
                {
                    outputWriter.Write(character);
                }
            }

            outputWriter.Flush();
        }

        public static void TransformBytesToHex(StreamReader contentReader, StreamWriter outputWriter)
        {
            while (!contentReader.EndOfStream)
            {
                string character = contentReader.Read().ToString("X2", CultureInfo.InvariantCulture);
                outputWriter.Write(character);
            }
        }

        public static void WriteLinesWithNumbers(StreamReader contentReader, StreamWriter outputWriter)
        {
            int i = 1;
            string? x;
            while (!contentReader.EndOfStream)
            {
                outputWriter.Write("00" + i + " ");
                i++;
                x = contentReader.ReadLine();
                outputWriter.Write(x);
                outputWriter.WriteLine();
                outputWriter.Flush();
            }
        }

        public static void RemoveWordsFromContentAndWrite(StreamReader contentReader, StreamReader wordsReader, StreamWriter outputWriter)
        {
            StringBuilder stringBuilder = new StringBuilder(contentReader.ReadToEnd());
            string[] allWords = wordsReader.ReadToEnd().Split("\n");

            foreach (string word in allWords)
            {
                stringBuilder = stringBuilder.Replace(word, string.Empty);
            }

            outputWriter.Write(stringBuilder);
        }
    }
}
