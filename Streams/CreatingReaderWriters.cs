using System.Globalization;
using System.Text;

[assembly: CLSCompliant(true)]

namespace Streams
{
    public static class CreatingReaderWriters
    {
        public static StringReader CreateStringReader(string str)
        {
            StringReader sr = new StringReader(str);
            return sr;
        }

        public static StringWriter CreateStringWriter()
        {
            StringWriter sw = new StringWriter();
            return sw;
        }

        public static StringWriter CreateStringWriterThatWritesToStringBuilder(StringBuilder stringBuilder)
        {
            StringWriter sw = new StringWriter(stringBuilder);
            return sw;
        }

        public static StringWriter CreateStringWriterThatWritesCultureSpecificData(CultureInfo cultureInfo)
        {
            StringWriter sw = new StringWriter(cultureInfo);
            return sw;
        }

        public static StreamReader CreateStreamReaderFromStream(Stream stream)
        {
            StreamReader sr = new StreamReader(stream);
            return sr;
        }

        public static StreamWriter CreateStreamWriterToStream(Stream stream)
        {
            StreamWriter sw = new StreamWriter(stream);
            return sw;
        }
    }
}
