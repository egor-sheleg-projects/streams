using System.Text;

namespace Streams
{
    public static class ReadingFromStream
    {
        public static string ReadAllStreamContent(StreamReader streamReader)
        {
            return streamReader.ReadToEnd();
        }

        public static string[] ReadLineByLine(StreamReader streamReader)
        {
            List<string> list = new List<string>();

            string? line;
            while ((line = streamReader.ReadLine()) != null)
            {
                list.Add(line);
            }

            return list.ToArray();
        }

        public static StringBuilder ReadOnlyLettersAndNumbers(StreamReader streamReader)
        {
            StringBuilder res = new StringBuilder();

            do
            {
                char charLines = (char)streamReader.Peek();
                if (char.IsLetterOrDigit(charLines))
                {
                    charLines = (char)streamReader.Read();
                    res = res.Append(charLines);
                }
                else
                {
                    break;
                }
            }
            while (!streamReader.EndOfStream);

            return res;
        }

        public static char[][] ReadAsCharArrays(StreamReader streamReader, int arraySize)
        {
            long arrLength;

            if (streamReader.BaseStream.Length % arraySize == 0)
            {
                arrLength = streamReader.BaseStream.Length / arraySize;
            }
            else
            {
                arrLength = (streamReader.BaseStream.Length / arraySize) + 1;
            }

            char[][] array = new char[arrLength][];

            for (int i = 0; i < array.Length; i++)
            {
                long chunkSize = arraySize;

                if (i == array.Length - 1 && streamReader.BaseStream.Length % arraySize != 0)
                {
                    chunkSize = streamReader.BaseStream.Length % arraySize;
                }

                array[i] = new char[chunkSize];

                for (int j = 0; j < chunkSize; j++)
                {
                    array[i][j] = (char)streamReader.Read();
                }
            }

            return array;
        }
    }
}
