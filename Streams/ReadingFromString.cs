namespace Streams
{
    public static class ReadingFromString
    {
        public static string ReadAllStreamContent(StringReader stringReader)
        {
            return stringReader.ReadToEnd();
        }

        public static string? ReadCurrentLine(StringReader stringReader)
        {
            return stringReader.ReadLine();
        }

        public static bool ReadNextCharacter(StringReader stringReader, out char currentChar)
        {
            currentChar = (char)stringReader.Read();
            if (char.IsLetterOrDigit(currentChar) || char.IsPunctuation(currentChar))
            {
                return true;
            }

            currentChar = ' ';
            return false;
        }

        public static bool PeekNextCharacter(StringReader stringReader, out char currentChar)
        {
            currentChar = (char)stringReader.Peek();
            if (char.IsLetterOrDigit(currentChar) || char.IsPunctuation(currentChar))
            {
                return true;
            }

            currentChar = ' ';
            return false;
        }

        public static char[] ReadCharactersToBuffer(StringReader stringReader, int count)
        {
            char[] buffer = new char[count];

            for (int i = 0; i < count; i++)
            {
                buffer[i] = (char)stringReader.Read();
            }

            return buffer;
        }
    }
}
